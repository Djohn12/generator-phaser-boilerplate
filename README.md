# What is it

This project is a yeoman-generator which will give you a Phaser3 boilerplate.
The boilerplates generated use parcel-bundler to compile and serve the application.

The project is largely inspired from https://github.com/samme/phaser-parcel and https://github.com/rblopes/generator-phaser-plus (the last not being maintained anymore)


# How to use it

## Boilerplate and generation

1. Create the directory which will host your new game.

		mkdir my-new-awesome-game
		cd my-new-awesome-game

2. Run phaser-boilerplate and answer the some questions (or don't).

		phaser-boilerplate


## Running the new project

1. Once your boilerplate is generated you'll need to install the dependencies needed.

		npm install


2. You're now set up and can already make use of some commands.

		# compile game into dist/ folder, watch all files and launch a server on port 1234
		npm run start

		# compile for production
		npm run build

		# remove all content from dist/
		npm run clean

		# run eslint on project using .eslintrc.js file
		npm run test

See [Parcel-bundler documentation](https://parceljs.org/cli.html) if you want to update package.json scripts to better suit your needs.