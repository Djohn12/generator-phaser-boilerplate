'use strict';

const Generator = require('yeoman-generator');
const chalk = require('chalk');
// const templatesConfig = require('./defaultConfig');
module.exports = class extends Generator {
	constructor(args, opt) {
		super(args, opt);
	}

	initializing() {
		this.log([
			chalk.green('='.repeat(100)),
			chalk.white(`                                     It's time to get started !`),
      chalk.white(`            Answer few questions if you want the generator to fill in some configuration`),
			chalk.green('='.repeat(100)),
			].join('\n'));
	}


	async prompting() {
		this.variables = await this.prompt([{
			name: 'title',
			message: `First, give your new game a cool name (used for package.json && html title)`,
      default: `My awesome game`,
      filter: input => input.trim(),
      validate: (input) => {
      	if (input === "") {
      		return 'No way! Give your game a name !!';
      	}
      	else {
      		return true;
      	}
			}
		}, {
      name: 'description',
      message: `How would you describe you game project (optional, used for package.json)`,
      filter: input => input.trim()
	  }]);
	}



	writing() {
		const {variables} = this;

		this.fs.copyTpl(
      this.templatePath(),
      this.destinationPath(),
      variables,
      {},
      {
        globOptions: {
          dot: true
        }
      },
      console.log(this.destinationRoot())
    );

    // Just rename `.gitignore` after copying it to the project directory.
    // this.fs.move(
      // this.destinationPath('gitignore'),
      // this.destinationPath('.gitignore')
    // );


   //  Set default configuration values.
    // this.config.templatesConfig({
    //   createdWith: this.rootGeneratorVersion(),
    //   creationDate: new Date().toISOString(),
    //   ...templatesConfig
    // });
	}

};
